class Player extends Creature {
  AnimationImage image_right;
  AnimationImage image_left;

  Player(float grid_x, float grid_y, float speed) {
    super(WorldTypes.PLAYER, grid_x, grid_y, speed);
    image_right = new AnimationImage("images/player/walk_right", 3, ANIMATION_SPEED);
    image_left = new AnimationImage("images/player/walk_left", 3, ANIMATION_SPEED);
    setImageContainer(image_right);
  }

  @Override
  void move() {
    super.move();
    
    float move_x = 0;
    float move_y = 0;
    
    if (direction.equals(CreatureDirections.UP)) {
      move_y = -speed;
    } else if (direction.equals(CreatureDirections.DOWN)) {
      move_y = speed;
    } else if (direction.equals(CreatureDirections.LEFT)) {
      move_x = -speed;
    } else if (direction.equals(CreatureDirections.RIGHT)) {
      move_x = speed;
    }

    CollisionResult collision_result = checkCollision(move_x, move_y);
    if (collision_result != null) {
      if (collision_result.type.equals(WorldTypes.WALL_BRICK)) {
        return;
      } else if (collision_result.type.equals(WorldTypes.COIN)) {
        world_objects.remove(collision_result.id);
        return;
      }
    } else {
      pixel_x += move_x;
      pixel_y += move_y;
      return;
    }
  }

  @Override
  void update() {
    if (key_handler.up_pressed) {
      changeDirection(CreatureDirections.UP);
    } else if (key_handler.down_pressed) {
      changeDirection(CreatureDirections.DOWN);
    } else if (key_handler.left_pressed) {
      changeDirection(CreatureDirections.LEFT);
      setImageContainer(image_left);
    } else if (key_handler.right_pressed) {
      changeDirection(CreatureDirections.RIGHT);
      setImageContainer(image_right);
    }
  }
}